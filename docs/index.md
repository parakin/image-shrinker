---
title: image_shrinker
summary: Shrinks images; use before committing big files into git.
author: Don Parakin
date: 2023-11-18
---

# image_shrinker

Shrinks images. Use it before committing to git and/or using on a website.

(Docs are coming soon. Gotta finalize the code before I document it.)

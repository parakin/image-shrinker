
# A Pinch of Python

The *image-shrinker* script that you must write is written in the
[Python](https://www.python.org/) programming language.

Keep calm and read on.

Although it will technically be a Python program,
you need to know extremely little about Python programming
to create your script and use *image-shrinker*.

Breath in, breath out, breath in, breath out.
That's it. Now keep reading.

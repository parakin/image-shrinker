"""Shrinks images. Use it before committing to git and/or using on a website"""

__version__ = "0.1.0"

from .shrinker import *

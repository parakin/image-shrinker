
from unittest.mock import patch
import unittest, io
import image_shrinker
from image_shrinker import Shrinker

class TestShrinkerConfigs(unittest.TestCase):
    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_config_default(self, stdout):
        s = Shrinker(__file__, testrun=True)
        s.for_subdir('', width=111)
        self.assertEqual(111, s._configs[''].width)

    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_config_dir_prefixes_suffixes(self, stdout):
        s = Shrinker(__file__, testrun=True)
        s.for_subdir('a/b/c', width=111)
        self.assertEqual(111, s._configs['a/b/c'].width)
        s.for_subdir('/a/b/c/', width=222)
        self.assertEqual(222, s._configs['a/b/c'].width)
        s.for_subdir('./a/b/c/', width=333)
        self.assertEqual(333, s._configs['a/b/c'].width)

    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_config_dir_not_exist(self, stdout):
        s = Shrinker(__file__, testrun=True)
        s.for_subdir('test-images-src/does/not/exist')
        out = stdout.getvalue()
        self.assertIn('ERROR:', out)
        self.assertIn('Not found', out)
        self.assertTrue(s._halting)

    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_config_dir_is_a_file(self, stdout):
        s = Shrinker(__file__, testrun=True)
        s.for_subdir('test-images-src/pietro-de-grandi-T7K4aEPoGGk-unsplash.jpg')
        out = stdout.getvalue()
        self.assertIn('ERROR:', out)
        self.assertIn('not a directory', out)
        self.assertTrue(s._halting)

    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_config_dir_exists(self, stdout):
        test_dir = 'test-images-src/dir1'
        s = Shrinker(__file__, testrun=True)
        s.for_subdir(test_dir)
        out = stdout.getvalue()
        self.assertNotIn('ERROR:', out)
        self.assertFalse(s._halting)
        self.assertIn(test_dir, s._configs)

    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_get_config_for_image(self, stdout):
        def test_it(img, width):
            cfg = s._get_config_for_image(img)
            self.assertEqual(width, cfg.width, f'img={img}')
        s = Shrinker(__file__, testrun=True)
        pre = 'test-images-src'
        s.for_subdir(pre+'/dir1', width=200)
        s.for_subdir(pre+'/dir2', width=300)
        s.for_subdir(pre+'/dir21', width=400)
        test_it('any/other/path/my.jpg', image_shrinker.DEFAULT_IMAGE_WIDTH)
        test_it(pre+'/dir21/my.jpg', 400)
        test_it(pre+'/dir1/under/some/level/my.jpg', 200)
        s.for_subdir('', width=2048)    # set for all of root
        test_it(pre+'/my.jpg', 2048)
        test_it(pre+'/any/path/to/my.jpg', 2048)

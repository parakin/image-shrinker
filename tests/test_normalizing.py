
from unittest.mock import patch
import unittest, io
from image_shrinker import Shrinker

class TestImageFinding(unittest.TestCase):
    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_normalizing_filepaths(self, stdout):
        s = Shrinker(__file__, testrun=True)
        def test_it(in_fpath, expected):
            self.assertEqual(expected, Shrinker._normalize_fn(in_fpath))

        test_it('aa\\bb\\cc\\', 'aa/bb/cc')
        test_it('\\aa\\bb\\cc', 'aa/bb/cc')
        test_it('.\\aa\\bb\\cc\\', 'aa/bb/cc')

        test_it('aa/bb/cc', 'aa/bb/cc')
        test_it('/aa/bb/cc/', 'aa/bb/cc')
        test_it('./aa/bb/cc', 'aa/bb/cc')
        test_it('./aa/bb/cc/', 'aa/bb/cc')

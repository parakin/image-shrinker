
from unittest.mock import patch
import unittest, io, sys
from image_shrinker import Shrinker

class TestShrinkerConfigs(unittest.TestCase):
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_no_args(self, stdout=None):
        with patch.object(sys, 'argv', ['script']):
            with self.assertRaises(SystemExit):
                s = Shrinker(__file__)
                out = stdout.getvalue()
                self.assertIn('usage:', out)    # Help text
                self.assertIn('arguments:', out)

    @patch('sys.stdout', new_callable=io.StringIO)
    def test_arg_shrink(self, stdout=None):
        with patch.object(sys, 'argv', ['script', '-s']):
            s = Shrinker(__file__)
            out = stdout.getvalue()
            self.assertNotIn('usage:', out)
            self.assertNotIn('arguments:', out)
            self.assertTrue(s.cmd_args.shrink)

    @patch('sys.stdout', new_callable=io.StringIO)
    def test_arg_quiet(self, stdout=None):
        with patch.object(sys, 'argv', ['script', '-s', '-q']):
            s = Shrinker(__file__)
            self.assertTrue(s.cmd_args.quiet)
        with patch.object(sys, 'argv', ['script', '-s', '--quiet']):
            s = Shrinker(__file__)
            self.assertTrue(s.cmd_args.quiet)

    @patch('sys.stdout', new_callable=io.StringIO)
    def test_arg_test(self, stdout=None):
        with patch.object(sys, 'argv', ['script', '-s', '-t']):
            s = Shrinker(__file__)
            self.assertTrue(True, s.cmd_args.test)
        with patch.object(sys, 'argv', ['script', '-s', '--test']):
            s = Shrinker(__file__)
            self.assertTrue(True, s.cmd_args.test)

    @patch('sys.stdout', new_callable=io.StringIO)
    def test_arg_noprompt(self, stdout=None):
        with patch.object(sys, 'argv', ['script', '-s', '--noprompt']):
            s = Shrinker(__file__)
            self.assertTrue(True, s.cmd_args.noprompt)

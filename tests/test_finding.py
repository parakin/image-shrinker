
from unittest.mock import patch
import unittest, io
from image_shrinker import Shrinker

class TestImageFinding(unittest.TestCase):
    @patch('sys.argv', new=['xxx', '--shrink'])
    @patch('sys.stdout', new_callable=io.StringIO)
    def test_find_images(self, stdout):
        s = Shrinker(__file__, testrun=True)
        imgs = s._find_all_image_files()
        pre = 'test-images-src'
        self.assertIn(pre+'/pietro-de-grandi-T7K4aEPoGGk-unsplash.jpg', imgs)
        self.assertIn(pre+'/dir2/dir21/agathe-BU1N6sGKTCo-unsplash.jpg', imgs)
        self.assertNotIn(pre+'/dir1/of-course-its-not-in.jpg', imgs)

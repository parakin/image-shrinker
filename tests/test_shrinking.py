
from tempfile import TemporaryDirectory
from unittest.mock import patch
import unittest, io
from image_shrinker import Shrinker

class TestImageFinding(unittest.TestCase):
    def setUp(self):
        # self.tmp_dir = TemporaryDirectory(prefix='image-shrinker-')
        pass

    def tearDown(self):
        # del self.tmp_dir
        pass

    @patch('sys.argv', new=['xxx', '--shrink', '--noprompt'])
    # @patch('sys.stdout', new_callable=io.StringIO)
    def test_shrink_1(self, stdout=None):
        s = Shrinker(__file__, testrun=True)
        s.for_subdir('',
            width=800, shrinkable_kb=128)
        s.for_subdir('test-images-src/dir2/small',
            height=100, shrinkable_kb=64)
        s.do_shrinking()
